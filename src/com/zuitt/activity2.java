package com.zuitt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class activity2 {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        int[] numbers = new int[5];
        int[] primeNumbers = {2, 3, 5, 7, 11};

        int firstPrimeNumber = primeNumbers[0];
        System.out.println("The first prime number is : "+firstPrimeNumber);

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> toiletries = new HashMap<>();

        toiletries.put("toothpaste", 20);
        toiletries.put("toothbrush", 15);
        toiletries.put("soap", 12);

        System.out.println("Our current inventory is consist of: " + toiletries.toString());

    }

}
